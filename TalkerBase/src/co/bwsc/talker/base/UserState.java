package co.bwsc.talker.base;

public enum UserState {
	DEACTIVATED("Deactivated"), NORMAL("Normal"), BANNED("Banned");
	public final String state;

	private UserState(String state) {
		this.state = state;
	}

	public String toString() {
		return state;
	}

	public static UserState getState(String name) {
		for (UserState s : UserState.values()) {
			if (s.toString().equalsIgnoreCase(name)) {
				return s;
			}

		}
		throw new IllegalArgumentException();
	}
}
