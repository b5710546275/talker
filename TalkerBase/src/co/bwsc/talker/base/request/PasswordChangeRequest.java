package co.bwsc.talker.base.request;

public class PasswordChangeRequest implements Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6295110362114483290L;

	private final RequestType type = RequestType.PASSWORD_CHANGE;
	
	private String newPassword;
	
	public PasswordChangeRequest(String newPassword) {
		this.newPassword = newPassword;
	}
	
	@Override
	public String getRequestMessage() {
		return newPassword;
	}

	@Override
	public RequestType getType() {
		return type;
	}

}
