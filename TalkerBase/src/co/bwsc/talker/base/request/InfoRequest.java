package co.bwsc.talker.base.request;

public class InfoRequest implements Request {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1083804300381602135L;

	private final RequestType type = RequestType.INFO;
	private InfoRequestType infoType;
	private String requestMessage;

	public InfoRequest(InfoRequestType infoType, String requestMessage) {
		this.infoType = infoType;
		this.requestMessage = infoType.toString() + "::" + requestMessage;
	}

	public InfoRequest(InfoRequestType infoType) {
		this.infoType = infoType;
		this.requestMessage = infoType.toString();
	}

	public InfoRequestType getInfoType() {
		return infoType;
	}

	@Override
	public String getRequestMessage() {
		return requestMessage;
	}

	@Override
	public RequestType getType() {
		return type;
	}
}
