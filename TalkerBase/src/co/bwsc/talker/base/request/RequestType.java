package co.bwsc.talker.base.request;

public enum RequestType {
	LOGIN, INFO, ADD_FRIEND, SEND_MESSAGE, PROFILE_UPDATE, PASSWORD_CHANGE,ADD_CHAT;
}