package co.bwsc.talker.base.request;

public class LoginRequest implements Request {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8247678460486309926L;
	private final RequestType type = RequestType.LOGIN;
	private String username, password;
	public LoginRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}
	@Override
	public String getRequestMessage() {
		return username + "::" + password;
	}
	@Override
	public RequestType getType() {
		return type;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	
}
