package co.bwsc.talker.base.request;

import java.io.Serializable;

public interface Request extends Serializable {
	String getRequestMessage();
	RequestType getType();
}