package co.bwsc.talker.base.request;

public class AddFriendRequest implements Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1101160405951977560L;

	private final RequestType type = RequestType.ADD_FRIEND;
	
	private int targetUserId;
	
	public AddFriendRequest(int targetUserId) {
		this.targetUserId = targetUserId;
	}
	
	public int getTargetUserId() {
		return targetUserId;
	}
	
	@Override
	public String getRequestMessage() {
		return Integer.toString(targetUserId);
	}

	@Override
	public RequestType getType() {
		return type;
	}

}
