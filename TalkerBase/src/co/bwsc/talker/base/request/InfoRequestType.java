package co.bwsc.talker.base.request;

public enum InfoRequestType {
	USER, PROFILE, FRIENDS, CHATS, MESSAGE, MESSAGES_IN_CHAT, USERS_IN_CHAT, MEDIA_IN_MESSAGE;
}
