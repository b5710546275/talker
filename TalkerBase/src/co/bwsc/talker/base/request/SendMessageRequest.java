package co.bwsc.talker.base.request;

import co.bwsc.talker.base.Message;

public class SendMessageRequest implements Request {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8105733336822804378L;

	private final RequestType type = RequestType.SEND_MESSAGE;
	
	private Message message;
	
	private int targetChatId;
	
	
	public SendMessageRequest(int targetChatId, Message message) {
		this.targetChatId = targetChatId;
		this.message = message;
	}
	
	@Override
	public String getRequestMessage() {
		return targetChatId + "::" + message;
	}

	@Override
	public RequestType getType() {
		return type;
	}

	public Message getMessage() {
		return message;
	}

	public int getTargetChatId() {
		return targetChatId;
	}
	
	

}
