package co.bwsc.talker.base.response;

public enum MessageOneResponseStatus implements ResponseStatus{
	NORMAL, UNAUTHORIZED, FAIL;
}
