package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Message;

public class MessageResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.MESSAGE;
	private MessageResponseStatus responseStatus;
	private Message[] responseObject;

	public MessageResponse(Message[] responseObject, MessageResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}
 
	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public MessageResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public Message[] getResponseObject() {
		return responseObject;
	}

}
