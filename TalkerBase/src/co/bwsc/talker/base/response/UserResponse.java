package co.bwsc.talker.base.response;

import co.bwsc.talker.base.User;

public class UserResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.USER;
	private UserResponseStatus responseStatus;
	private User responseObject;

	public UserResponse(User responseObject, UserResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}

	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public UserResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public User getResponseObject() {
		return responseObject;
	}

}
