package co.bwsc.talker.base.response;

public enum ProfileResponseStatus implements ResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
