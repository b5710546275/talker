package co.bwsc.talker.base.response;

public enum PasswordChangeResponseStatus implements ResponseStatus {
	CHANGE_SUCCESSFUL, ILLEGAL_CHARACTER, SIMILAR_TO_OLD, CHANGE_FAILED;
}
