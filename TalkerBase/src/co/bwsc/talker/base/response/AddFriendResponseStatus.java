package co.bwsc.talker.base.response;

public enum AddFriendResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
