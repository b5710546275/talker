package co.bwsc.talker.base.response;

public enum UserResponseStatus implements ResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
