package co.bwsc.talker.base.response;

public enum ResponseType {
	LOGIN, PASSWORD_CHANGE, USER, PROFILE, CHAT, MESSAGE, MEDIA;
}
