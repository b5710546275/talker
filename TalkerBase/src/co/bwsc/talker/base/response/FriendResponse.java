package co.bwsc.talker.base.response;

public class FriendResponse implements Response{
	private int[] friendIDs;
	private FriendResponseStatus friendResponseStatus;
	public FriendResponse(int[] friendIDs, FriendResponseStatus friendResponseStatus){
		this.friendIDs = friendIDs;
		this.friendResponseStatus = friendResponseStatus;
	}
	@Override
	public String getResponseMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FriendResponseStatus getResponseStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] getResponseObject() {
		return friendIDs;
	}

	@Override
	public ResponseType getType() {
		// TODO Auto-generated method stub
		return null;
	}

	public int[] getFriendIDs() {
		return friendIDs;
	}

	public void setFriendIDs(int[] friendIDs) {
		this.friendIDs = friendIDs;
	}
	
}
