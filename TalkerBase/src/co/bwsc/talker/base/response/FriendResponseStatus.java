package co.bwsc.talker.base.response;

public enum FriendResponseStatus implements ResponseStatus{
	NORMAL, UNAUTHORIZED, FAIL;
}
