package co.bwsc.talker.base.response;

public enum LoginResponseStatus implements ResponseStatus {
	AUTH_OKAY, AUTH_FAIL_USER, AUTH_FAIL_PASSWORD, SERVICE_UNAVAILABLE;
}