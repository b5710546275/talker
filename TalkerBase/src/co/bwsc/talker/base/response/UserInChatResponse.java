package co.bwsc.talker.base.response;

public class UserInChatResponse implements Response {
	private UserInChatResponseStatus UserInChatResponseStatus;
	private int[] userInChat;
	public UserInChatResponse(int[] userInChat, UserInChatResponseStatus userInChatResponseStatus) {
		this.userInChat = userInChat;
		this.UserInChatResponseStatus = userInChatResponseStatus;
	}
	@Override
	public String getResponseMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserInChatResponseStatus getResponseStatus() {
		return UserInChatResponseStatus;
	}

	@Override
	public int[] getResponseObject() {
		return userInChat;
	}

	@Override
	public ResponseType getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
