package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Chat;

public class ChatResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.CHAT;
	private ChatResponseStatus responseStatus;
	private Chat[] responseObject;

	public ChatResponse(Chat[] responseObject, ChatResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}

	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public ChatResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public Chat[] getResponseObject() {
		return responseObject;
	}

}
