package co.bwsc.talker.base.response;

public enum MediaResponseStatus implements ResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
