package co.bwsc.talker.base.response;

import java.io.Serializable;

public interface Response<T,K> extends Serializable {
	String getResponseMessage();
	T getResponseStatus();
	K getResponseObject();
	ResponseType getType();
}
