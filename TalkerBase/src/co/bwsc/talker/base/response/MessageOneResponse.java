package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Message;

public class MessageOneResponse implements Response{
	private Message message;
	private MessageOneResponseStatus messageOneResponseStatus;
	
	public MessageOneResponse(Message message, MessageOneResponseStatus messageOneResponseStatus) {
		this.message = message;
		this.messageOneResponseStatus = messageOneResponseStatus;
	}
	@Override
	public String getResponseMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageOneResponseStatus getResponseStatus() {
		// TODO Auto-generated method stub
		return messageOneResponseStatus;
	}

	@Override
	public Message getResponseObject() {
		return message;
	}

	@Override
	public ResponseType getType() {
		// TODO Auto-generated method stub
		return null;
	}
	public Message getMessage() {
		return message;
	}
	
}
