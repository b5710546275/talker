package co.bwsc.talker.base.response;

public enum UserInChatResponseStatus implements ResponseStatus{
	NORMAL, UNAUTHORIZED, FAIL;
}
