package co.bwsc.talker.base.response;

public enum ChatResponseStatus implements ResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
