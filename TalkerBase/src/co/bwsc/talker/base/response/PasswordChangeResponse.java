package co.bwsc.talker.base.response;

public class PasswordChangeResponse implements Response {
	private static final long serialVersionUID = 8873614465748785319L;

	private final ResponseType type = ResponseType.PASSWORD_CHANGE;
	private PasswordChangeResponseStatus passwordChangeStatus;
	public PasswordChangeResponse(PasswordChangeResponseStatus passwordChangeStatus) {
		this.passwordChangeStatus = passwordChangeStatus;
	}
	
	@Override
	public String getResponseMessage() {
		return passwordChangeStatus.toString();
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public PasswordChangeResponseStatus getResponseStatus() {
		return passwordChangeStatus;
	}

	@Override
	public Object getResponseObject() {
		return null;
	}

}
