package co.bwsc.talker.base.notification;

public interface Notification {
	String getNotificationMessage();
	Object getNotificationObject();
	NotificationType getType();
}
