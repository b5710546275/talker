package co.bwsc.talker.base.notification;

import co.bwsc.talker.base.Message;

public class NewMessageNotification implements Notification {

	private final NotificationType type = NotificationType.NEW_MESSAGE;
	private Message message;

	public NewMessageNotification(Message message) {
		this.message = message;
	}

	@Override
	public String getNotificationMessage() {
		return "New message notification: " + message.toString();
	}

	@Override
	public NotificationType getType() {
		return type;
	}

	@Override
	public Object getNotificationObject() {
		return message;
	}

}
