package co.bwsc.talker.base.notification;

public enum NotificationType {
	NEW_MESSAGE, FRIEND_STATUS_CHANGE;
}
