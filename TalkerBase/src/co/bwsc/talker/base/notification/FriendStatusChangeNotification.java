package co.bwsc.talker.base.notification;

import co.bwsc.talker.base.UserStatus;

public class FriendStatusChangeNotification implements Notification {

	private final NotificationType type = NotificationType.FRIEND_STATUS_CHANGE;
	
	private int userID;
	private UserStatus status;
	
	public FriendStatusChangeNotification(int userID, UserStatus status) {
		this.userID = userID;
		this.status = status;
	}
	
	@Override
	public String getNotificationMessage() {
		return "Friend Status Change notification: " + userID + "'s status changed to " + status.toString();
	}

	@Override
	public Object getNotificationObject() {
		return status;
	}

	@Override
	public NotificationType getType() {
		return type;
	}

}
