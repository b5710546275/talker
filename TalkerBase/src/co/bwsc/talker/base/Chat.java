package co.bwsc.talker.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Chat implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3088098458060143923L;
	private int chatID;
	private List<Integer> userIDs = new ArrayList<Integer>();
	private List<Message> messages = new ArrayList<Message>();
	private List<Integer> messageIDs = new ArrayList<Integer>();
	private List<String> userNames = new  ArrayList<String>();
	public Chat() {
	}

	public Chat(int chatID) {
		this.chatID = chatID;
	}

	public List<Integer> getUserIDs() {
		return userIDs;
	}

	public void setUserIDs(List<Integer> userIDs) {
		this.userIDs = userIDs;
	}

	public void setUserIDs(int[] usetIDs) {
		this.userIDs.removeAll(userIDs);
		this.userIDs.addAll(userIDs);
	}

	public List<Integer> getMessageIDs() {
		return messageIDs;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessageIDs(List<Integer> messageIDs) {
		this.messageIDs = messageIDs;
	}

	public void setMessageIDs(int[] messagesIDs) {
		this.messageIDs.clear();
		this.messageIDs.addAll(messageIDs);
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public void setMessages(Message[] messages) {
		this.messages.clear();
		this.messages.addAll(Arrays.asList(messages));
	}

	public void addUser(Integer userID) {
		this.userIDs.add(userID);
	}

	public void removeUser(User user) {
		this.userIDs.remove(user);
	}

	public void removeUser(int userID) {
		for (int u : userIDs) {
			if (u == userID) {
				removeUser(u);
			}
		}
	}

	public void addMessageID(Integer message) {
		this.messageIDs.add(message);
	}

	public int getChatID() {
		return chatID;
	}

	public void setChatID(int chatID) {
		this.chatID = chatID;
	}

	public List<String> getUserNames() {
		return userNames;
	}

	public void setUserNames(List<String> userNames) {
		this.userNames = userNames;
	}
	
	public void setUserNames(String[] userNames) {
		List list =  Arrays.asList(userNames);
		this.userNames = new ArrayList<String>();
		this.userNames.addAll(list);
	}
}
