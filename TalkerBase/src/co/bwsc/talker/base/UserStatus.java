package co.bwsc.talker.base;

public enum UserStatus {
	OFFLINE, AVAILABLE, AWAY, BUSY;
	public static UserStatus getStatus(String name) {
		for (UserStatus s : UserStatus.values()) {
			if (s.toString().equalsIgnoreCase(name)) {
				return s;
			}
		}
		throw new IllegalArgumentException();
	}
}
