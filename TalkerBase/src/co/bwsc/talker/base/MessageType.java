package co.bwsc.talker.base;

public enum MessageType {
	PLAIN(0), WITHMEDIA(1), NUDGE(2);
	public final int type;

	private MessageType(int type) {
		this.type = type;
	}
}
