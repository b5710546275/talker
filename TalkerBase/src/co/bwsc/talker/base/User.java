package co.bwsc.talker.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable{
	private static final long serialVersionUID = 5528966910773620354L;
	private int userID;
	private String userName;
	private UserState userState;
	private UserStatus userStatus;
	private int userProfileID;
	private List<Integer> friends = new ArrayList<Integer>();

	public User(int userID, String userName, UserState userState,
			UserStatus userStatus, int userProfileID) {
		this.userID = userID;
		this.userName = userName;
		this.userState = userState;
		this.userStatus = userStatus;
		this.userProfileID = userProfileID;
	}

	public User(String userName, UserState userState, UserStatus userStatus,
			int userProfileID) {
		super();
		this.userName = userName;
		this.userState = userState;
		this.userStatus = userStatus;
		this.userProfileID = userProfileID;
	}

	public List<Integer> getFriends() {
		return friends;
	}

	public void setFriends(List<Integer> friends) {
		this.friends = friends;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UserState getUserState() {
		return userState;
	}

	public void setUserState(UserState userState) {
		this.userState = userState;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public int getUserProfileID() {
		return userProfileID;
	}

	public void setUserProfileID(int userProfileID) {
		this.userProfileID = userProfileID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", userName=" + userName
				+ ", userState=" + userState + ", userStatus=" + userStatus
				+ ", userProfileID=" + userProfileID + ", friends=" + friends
				+ "]";
	}

}
