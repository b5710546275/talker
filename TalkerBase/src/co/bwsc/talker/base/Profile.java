package co.bwsc.talker.base;

import java.io.Serializable;

public class Profile implements Serializable{
	
	private static final long serialVersionUID = 5588946910773620354L;
	private int proFileID;
	private String profileDisplayName;
	private String profileFirstname;
	private String profileLastName;
	private String profileBio;
	private String profilePhotoURL;

	public Profile(int proFileID, String profileDisplayName,
			String profileFirstname, String profileLastName, String profileBio) {
		super();
		this.proFileID = proFileID;
		this.profileDisplayName = profileDisplayName;
		this.profileFirstname = profileFirstname;
		this.profileLastName = profileLastName;
		this.profileBio = profileBio;
	}

	public Profile(String profileDisplayName, String profileFirstname,
			String profileLastName, String profileBio, String profilePhotoURL) {
		super();
		this.profileDisplayName = profileDisplayName;
		this.profileFirstname = profileFirstname;
		this.profileLastName = profileLastName;
		this.profileBio = profileBio;
		this.profilePhotoURL = profilePhotoURL;
	}

	public Profile(int proFileID, String profileDisplayName,
			String profileFirstname, String profileLastName, String profileBio,
			String profilePhotoURL) {
		super();
		this.proFileID = proFileID;
		this.profileDisplayName = profileDisplayName;
		this.profileFirstname = profileFirstname;
		this.profileLastName = profileLastName;
		this.profileBio = profileBio;
		this.profilePhotoURL = profilePhotoURL;
	}

	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}

	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}

	public String getProfileDisplayName() {
		return profileDisplayName;
	}

	public void setProfileDisplayName(String profileDisplayName) {
		this.profileDisplayName = profileDisplayName;
	}

	public String getProfileLastName() {
		return profileLastName;
	}

	public void setProfileLastName(String profileLastName) {
		this.profileLastName = profileLastName;
	}

	public String getProfileBio() {
		return profileBio;
	}

	public void setProfileBio(String profileBio) {
		this.profileBio = profileBio;
	}

	public int getProFileID() {
		return proFileID;
	}

	public String getProfileFirstname() {
		return profileFirstname;
	}

	public void setProfileFirstname(String profileFirstname) {
		this.profileFirstname = profileFirstname;
	}

	public void setProFileID(int proFileID) {
		this.proFileID = proFileID;
	}

	@Override
	public String toString() {
		return "Profile [proFileID=" + proFileID + ", profileDisplayName="
				+ profileDisplayName + ", profileFirstname=" + profileFirstname
				+ ", profileLastName=" + profileLastName + ", profileBio="
				+ profileBio + ", profilePhotoURL=" + profilePhotoURL + "]";
	}

}
