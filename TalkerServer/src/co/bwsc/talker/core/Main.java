package co.bwsc.talker.core;

import java.io.IOException;

import co.bwsc.talker.server.Server;
/**This is main.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class Main implements Runnable{
	/**Initialize attributes.*/
	private Server server;
	private int serverPort= 5555;
	Main(){
		server = new Server(serverPort);
	}
	/**This is main method.*/
	public static void main(String[] args){
		Main main = new Main();
		System.out.println("server");
		main.run();
	}

	@Override
	/**This is run method.*/
	public void run() {
		try {
			/*Call sever to listen.*/
			server.listen();;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}