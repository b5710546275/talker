package co.bwsc.talker.handler;

import co.bwsc.talker.base.request.AddFriendRequest;
import co.bwsc.talker.base.response.AddFriendResponse;
import co.bwsc.talker.base.response.AddFriendResponseStatus;
import co.bwsc.talker.base.response.FriendResponse;
import co.bwsc.talker.base.response.FriendResponseStatus;
import co.bwsc.talker.base.response.Response;
import co.bwsc.talker.database.UserFriendsDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle add friend request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class AddFriendRequestHandler extends RequestHandler{
	UserFriendsDB UserFriendsDB = new UserFriendsDB();
	@Override
	public void handle(Object msg, ConnectionToClient client, RequestManager manager) {
		AddFriendRequest addFriendRequest = (AddFriendRequest) msg;
		int friendID = addFriendRequest.getTargetUserId();
		int userID = manager.getConnectionMap().get(client).getUserID();
		AddFriendResponseStatus user =  UserFriendsDB.insert(userID, friendID);
		AddFriendResponseStatus friend = UserFriendsDB.insert(friendID, userID);
		
		AddFriendResponseStatus status = AddFriendResponseStatus.FAIL;
		if(user == AddFriendResponseStatus.NORMAL && friend == AddFriendResponseStatus.NORMAL){
			status = AddFriendResponseStatus.NORMAL;
		}
		AddFriendResponse addFriendResponse = responseFactory.addFriendResponse(status);
		manager.sentToClient(addFriendRequest, client);
		
		int[] friendIDs = UserFriendsDB.getFriend(userID);
		FriendResponse userResponse = responseFactory.friendResponse(friendIDs, FriendResponseStatus.NORMAL);
		manager.sentToClient(userResponse, client);
		
		ConnectionToClient friendConnection = manager.getUsetConnection(friendID);
		if(friendConnection!= null){
			friendIDs = UserFriendsDB.getFriend(friendID);
			FriendResponse friendResponse = responseFactory.friendResponse(friendIDs, FriendResponseStatus.NORMAL);
			manager.sentToClient(friendResponse, friendConnection);
			
		}

	}

	
}
