package co.bwsc.talker.handler;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.User;
import co.bwsc.talker.base.response.AddFriendResponse;
import co.bwsc.talker.base.response.AddFriendResponseStatus;
import co.bwsc.talker.base.response.ChatResponse;
import co.bwsc.talker.base.response.ChatResponseStatus;
import co.bwsc.talker.base.response.FriendResponse;
import co.bwsc.talker.base.response.FriendResponseStatus;
import co.bwsc.talker.base.response.LoginResponse;
import co.bwsc.talker.base.response.LoginResponseStatus;
import co.bwsc.talker.base.response.MediaResponse;
import co.bwsc.talker.base.response.MediaResponseStatus;
import co.bwsc.talker.base.response.MessageOneResponse;
import co.bwsc.talker.base.response.MessageOneResponseStatus;
import co.bwsc.talker.base.response.MessageResponse;
import co.bwsc.talker.base.response.MessageResponseStatus;
import co.bwsc.talker.base.response.PasswordChangeResponse;
import co.bwsc.talker.base.response.PasswordChangeResponseStatus;
import co.bwsc.talker.base.response.ProfileResponse;
import co.bwsc.talker.base.response.ProfileResponseStatus;
import co.bwsc.talker.base.response.UserResponse;
import co.bwsc.talker.base.response.UserResponseStatus;
/**
 * This class is for collecting method.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ResponseFactory {
	
	/**
	 * To add friend response. 
	 * @param addFriendResponseStatus is status.
	 * @return AddFriendResponse object. 
	 */
	public AddFriendResponse addFriendResponse(AddFriendResponseStatus addFriendResponseStatus){
		return new AddFriendResponse(addFriendResponseStatus);
	}
	
	/**
	 * To call chat response. 
	 * @param chat is array of chat.
	 * @param chatResponseStatus is status.
	 * @return chatResponse object. 
	 */
	public ChatResponse chatResponse(Chat[] chat,ChatResponseStatus chatResponseStatus){
		return new ChatResponse(chat, chatResponseStatus);
	}
	
	/**
	 * To call login response. 
	 * @param loginResponseStatus is status.
	 * @return loginResponse object. 
	 */
	public LoginResponse loginResponse(LoginResponseStatus loginResponseStatus){
		return new LoginResponse(loginResponseStatus);
	}
	
	/**
	 * To call media response.
	 * @param url is directory file.
	 * @param mediaResponseStatus is status.
	 * @return MediaResponse object.
	 */
	public MediaResponse mediaResponse(String url, MediaResponseStatus mediaResponseStatus){
		return new MediaResponse(url, mediaResponseStatus);
	}
	
	/**
	 * To add message response. 
	 * @param message is array of message.
	 * @param messageResponseStatus is status.
	 * @return messageResponse object. 
	 */
	public MessageResponse messageResponse(Message[] message,MessageResponseStatus meResponseStatus) {
		return new MessageResponse(message, meResponseStatus);
	}
	
	/**
	 * To add password change response. 
	 * @param PasswordChangeResponseStatus is status.
	 * @return PasswordChangeResponse object. 
	 */
	public PasswordChangeResponse passwordChangeResponse(PasswordChangeResponseStatus passwordChangeResponseStatus){
		return new PasswordChangeResponse(passwordChangeResponseStatus);
	}
	
	/**
	 * To call profile response.
	 * @param profile is profile.
	 * @param profileResponseStatus is status.
	 * @return  ProfileResponse object.
 	 */
	public ProfileResponse profileResponse(Profile profile, ProfileResponseStatus profileResponseStatus){
		return new ProfileResponse(profile, profileResponseStatus);
	}
	
	/**
	 * To call user response. 
	 * @param user is user.
	 * @param userResponseStatus is status.
	 * @return UserResponse object.
	 */
	public UserResponse userResponse(User user,UserResponseStatus userResponseStatus){
		return new UserResponse(user, userResponseStatus);
	}
	
	/**
	 * To call Friend response.
	 * @param friendIDs is ID of friend.
	 * @param friendResponseStatus is status.
	 * @return FriendResponse object.
	 */
	public FriendResponse friendResponse(int[] friendIDs,FriendResponseStatus friendResponseStatus){
		return new FriendResponse(friendIDs,friendResponseStatus);
	}
	
	/**
	 * To call Message one response.
	 * @param message is message.
	 * @param messageOneResponseStatus is status.
	 * @return MessageOneResponse object.
	 */
	public MessageOneResponse messageOneResponse(Message message, MessageOneResponseStatus messageOneResponseStatus){
		return new MessageOneResponse(message, messageOneResponseStatus);
	}
}
