package co.bwsc.talker.handler;

import java.util.Arrays;

import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.request.SendMessageRequest;
import co.bwsc.talker.base.response.MessageOneResponse;
import co.bwsc.talker.base.response.MessageOneResponseStatus;
import co.bwsc.talker.base.response.MessageResponse;
import co.bwsc.talker.base.response.MessageResponseStatus;
import co.bwsc.talker.database.ChatMessageDB;
import co.bwsc.talker.database.MessageDB;
import co.bwsc.talker.database.UserChatDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle send message request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class SendMessageRequestHandler extends RequestHandler {
	MessageDB messageDB = new MessageDB();
	ChatMessageDB chatMessageDB = new ChatMessageDB();
	UserChatDB userChatDB = new UserChatDB();
	@Override
	public void handle(Object msg, ConnectionToClient client, RequestManager manager) {

		SendMessageRequest sendMessageRequest =(SendMessageRequest) msg;
		Message message = sendMessageRequest.getMessage();
		
		int chatID = sendMessageRequest.getTargetChatId();
		messageDB.insertMessage(sendMessageRequest.getMessage());
		int messageID = messageDB.getLastestMessageID();
		message.setMessageID(messageID);
		chatMessageDB.insert(chatID, messageID);
		int[] userIDs = userChatDB.getUsers(chatID);

		MessageOneResponseStatus messageOneResponseStatus = MessageOneResponseStatus.FAIL;
		if(message!= null){
			messageOneResponseStatus = MessageOneResponseStatus.NORMAL;
		}
		MessageOneResponse messageResponse = new MessageOneResponse(message, messageOneResponseStatus);
		notifyReciver(userIDs, manager, messageResponse);
	}
	
	/**
	 * To notify reciver.
	 * @param ids is ID.
	 * @param manager is who is send data.
	 * @param messageOneResponse is message to response.
	 */
	public void notifyReciver(int[] ids,RequestManager manager,MessageOneResponse messageOneResponse){
		for(int i : ids){
			if(manager.getUsetConnection(i)!=null){
				manager.sentToClient(messageOneResponse,manager.getUsetConnection(i));
			}
		}
	}
}
