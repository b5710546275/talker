package co.bwsc.talker.handler;

import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * This class is for handle request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 * @param <T> is general data type.
 */
public abstract class RequestHandler<T> {
	ResponseFactory responseFactory = new ResponseFactory();
	/**
	 * To handle request.
	 * @param msg is message.
	 * @param client is client.
	 * @param manager is manager.
	 */
	public abstract void handle(Object msg, ConnectionToClient client, RequestManager manager);
}
