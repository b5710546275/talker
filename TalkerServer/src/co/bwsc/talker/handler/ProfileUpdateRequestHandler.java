package co.bwsc.talker.handler;

import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.request.ProfileUpdateRequest;
import co.bwsc.talker.base.response.ProfileResponse;
import co.bwsc.talker.base.response.ProfileResponseStatus;
import co.bwsc.talker.database.ProfileDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle profile update request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ProfileUpdateRequestHandler extends RequestHandler{
	ProfileDB profileDB = new ProfileDB();
	@Override
	public void handle(Object msg, ConnectionToClient client, RequestManager manager) {
		ProfileUpdateRequest profileRequest = (ProfileUpdateRequest)msg;
		 Profile profile = profileRequest.getProfile();
		 profileDB.updateProfile(profile);
		 manager.sentToClient("success", client);
		 
		 ProfileResponseStatus profileResponseStatus = ProfileResponseStatus.FAIL;
		 if(profile !=null){
			 profileResponseStatus = ProfileResponseStatus.NORMAL;
		 }
		 ProfileResponse profileResponse = new ProfileResponse(profile, profileResponseStatus);
		 manager.sentToClient(profileResponse, client);
	}

}
