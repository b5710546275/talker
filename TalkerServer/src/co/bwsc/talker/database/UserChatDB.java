package co.bwsc.talker.database;

import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.User;
/**This is user chat database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class UserChatDB {
	private DatabaseFacade databaseFacade = new DatabaseFacade();
	private String tableName = "user_chat";
	private String format = "userName,userID,chatID";
	
	public UserChatDB() {
		 
	}
	
	/**To create the table.*/
	public void createTable(){
		String header = "id INTEGER PRIMARY KEY AUTOINCREMENT, userName TEXT NOT NULL, userID INTEGER NOT NULL, chatID INTEGER NOT NULL";
		databaseFacade.createTable(tableName, header);
	}
	
	/**
	 * To insert data into databaseFacade.
	 * @param user is User.
	 * @param chat is Chat.
	 */
	public void insert(User user, Chat chat){
	String userName = user.getUserName();
	int userID = user.getUserID();
	int chatID = chat.getChatID();	
	databaseFacade.insert(tableName, format, suround(userName)+","+userID+","+chatID);
	}
	
	/**
	 * To get data of user.
	 * @param chatID is ID that want to get.
	 * @return data of user.
	 */
	public int[] getUsers(int chatID){
		List<Map> list = databaseFacade.select(tableName, "userID", "WHERE chatID = "+chatID);
		return getIDArray(list,"userID");
	}
	public String[] getUserNames(int chatID){
		List<Map> list = databaseFacade.select(tableName, "userName", "WHERE chatID = "+chatID);
		String[] names = new String[list.size()];
		for(int i =0;i<list.size();i++){
			names[i] = (String)list.get(i).get("userName");
		}
		return names;
	}
	/**
	 * To get data of chat.
	 * @param userID is ID that want to get.
	 * @return data of chat.
	 */
	public int[] getChat(int userID){
		List<Map> list = databaseFacade.select(tableName, "chatID", "WHERE userID = "+userID);
		return getIDArray(list,"chatID");
	}
	
	/**
	 * To get the array of ID.
	 * @param list that want to get.
	 * @param field that want to get.
	 * @return array of ID.
	 */
	public int[] getIDArray(List<Map> list,String field){
		int[] ids = new int[list.size()];
		for(int i =0;i<list.size();i++){
			ids[i] = (int) list.get(i).get(field);
		}
		return ids;
	}
	
	/**
	 * To put single quote over message.
	 * @param s is the String.
	 * @return String the have quote over message.
	 */
	private String suround(String input){
		return "'"+input+"'";
	}
}
