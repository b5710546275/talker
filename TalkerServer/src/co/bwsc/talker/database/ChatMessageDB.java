package co.bwsc.talker.database;

import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Message;

/**This is database for chatMessage.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ChatMessageDB {
	/**Initialize attributes.*/
	private String header = "chatID INTEGER NOT NULL, messageID INTEGER NOT NULL";
	private String tableName = "chat_message";
	private String format = "chatID, messageID";
	private DatabaseFacade databaseFacade = new DatabaseFacade();
	
	/**To create table in database*/
	public void createTable(){
		databaseFacade.createTable(tableName, header);
	}
	
	/**To insert data to database.*/
	public void insert(Chat chat,Message message){
		databaseFacade.insert(tableName, format, chat.getChatID()+","+message.getMessageID());
	}
	/**To insert data to database.*/
	public void insert(int chatID,int messageID){
		databaseFacade.insert(tableName,format, chatID+","+messageID);
	}
	
	/**
	 * To get the messages.
	 * @param chatID is individual ID.
	 * @return the messages.
	 */
	public int[] getMessages(int chatID){
		List<Map> list = databaseFacade.select(tableName, "messageID", "WHERE chatID = "+chatID);
		int[] messages = new int[list.size()];
		for(int i =0;i<list.size();i++){
			messages[i] = (int) list.get(i).get("messageID");
		}
		return messages;
	}
}
