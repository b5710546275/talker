package co.bwsc.talker.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.Profile;

/**This is profile database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ProfileDB implements Database{
	private String format = "profileDisplayName,profileFirstname,profileLastname,profileBio,profilePhotoURL";
	private String feild ="profileID INTEGER PRIMARY KEY AUTOINCREMENT,profileDisplayName TEXT NOT NULL, profileFirstname TEXT NOT NULL, profileLastname TEXT NOT NULL, profileBio TEXT,  profilePhotoURL TEXT";
	private String tableName = "profile";
	DatabaseFacade databaseFacade;
	Connection c;
	public ProfileDB(){
		this.databaseFacade = new DatabaseFacade();
		this.c = databaseFacade.connectToDB();
	}
	
	/**To insert the profile.*/
	public void insertProfile(Profile profile){
		String value = "";
		value += suround(profile.getProfileDisplayName());
		value += suround(profile.getProfileFirstname());
		value += suround(profile.getProfileLastName());
		value += suround(profile.getProfileBio());
		value += "'"+profile.getProfilePhotoURL()+"'";
		databaseFacade.insert( tableName, format, value);
	}
	
	/**To create the table.*/
	public void createTable(){
		databaseFacade
		.createTable(tableName,feild);

	}
	
	/**To put single quote over message.*/
	public String suround(String s){
		return "'"+s+"',";
	}
	
	/**
	 * To get the profile.
	 * @param id is id that want to get profile.
	 * @return profile.
	 */
	public Profile getProfile(int id){
		List<Map> list = databaseFacade.select( tableName, "*", "WHERE profileID ="+id);
		Profile profile = null;
		if(list.size()>0){
			Map m = list.get(0);
				profile = new Profile(id,(String)m.get("profileDisplayName"),(String)m.get("profileFirstname"),(String)m.get("profileLastname"),(String)m.get("profileBio"),(String)m.get("profilePhotoURL"));
			System.out.println(profile);
		}
			return profile;
	}
	
	/**To update the profile.*/
	public void updateProfile(Profile profile){
		int id = profile.getProFileID();
		String profileDisplayName = profile.getProfileDisplayName();
		String profileFirstname = profile.getProfileFirstname();
		String profileLastName = profile.getProfileLastName();
		String profileBio = profile.getProfileBio();
		String profilePhotoURL = profile.getProfilePhotoURL();
		String s = "profileDisplayName ='"+profileDisplayName+"', profileFirstname ='"+
				profileFirstname+"', profileLastName = '"+profileLastName+"', profileBio ='"+
				profileBio+"', profilePhotoURL ='"+profilePhotoURL+"'";
		databaseFacade.update( tableName, s, "Where profileID ="+id);
	}
}
