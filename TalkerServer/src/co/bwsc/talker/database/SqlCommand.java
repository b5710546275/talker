 package co.bwsc.talker.database;

 /**This is SqulCommand to create the command.
  * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
  */
enum SqlCommand{
	CREATE("CREATE"),
	INSERT("INSERT"),
	INSERT_INTO("INSERT INTO"),
	SELECT("SELECT"),
	FROM("FROM"),
	DELETE("DELETE"),
	NULL("NULL"),
	WHERE("WHERE"),
	INTEGER("INTEGER"),
	REAL("REAL"),
	TEXT("TEXT"),
	BLOB("BLOB"),
	UPDATE("UPDATE"),
	TABLE("TABLE"),
	VALUES("VALUES"),
	KEY("KEY"),
	INT("INT"),
	SPACE(" "),
	SET("SET"),
	NOR_NULL("NOT NULL"),
	CHAR50("CHAR(50)"),
	BOOLEAN("BOOLEAN"),
	DATE("DATE"),
	DROP("DROP"),
	DATETIME("DATETIME"),
	INT_SPACE_PRIMARY_SPACE_KEY("INT PRIMARY KEY"),
	ORG_SQLITE_JDBC("org.sqlite.JDBC"),
	JDBC_COLON_SQLITE_COLOR("jdbc:sqlite:"),
	SIMICOLON(";"),
	OR("OR"),
	AND("AND"),
	BETWEEN("BETWEEN"),
	NOT("NOT"),
	LIKE("LIKE")
	;
	public final String name;
	SqlCommand(String name){
		this.name=name;
	}
	public String toString(){
		return name;
	}
}