package co.bwsc.talker.database;
import static co.bwsc.talker.base.response.LoginResponseStatus.AUTH_FAIL_PASSWORD;
import static co.bwsc.talker.base.response.LoginResponseStatus.AUTH_FAIL_USER;
import static co.bwsc.talker.base.response.LoginResponseStatus.AUTH_OKAY;

import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.response.LoginResponseStatus;

/**This is user password database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class UserPasswordDB {
	DatabaseFacade databaseFacade;
	String format = "userName,password";
	String tableName = "user_password";
	
	/**This is contractor of this class*/
	public UserPasswordDB() {
		databaseFacade = new DatabaseFacade();
	} 
	
	/**To create the table.*/
	public void createTable(){
		String header = "id INTEGER PRIMARY KEY AUTOINCREMENT,userName STRING NOT NULL, password TEXT NOT NULL ";
		databaseFacade.createTable(tableName, header);
	}
	
	/**
	 * To insert data into databaseFacade.
	 * @param userName is the user name.
	 * @param password is the password.
	 */
	public void insert(String userName, String password){
		databaseFacade.insertIfNotExists(tableName, format, surround(userName)+","+surround(password));
	}
	
	/**
	 * To get message with single quote.
	 * @param input is message.
	 * @return message with single quote.
	 */
	public String surround(String input){
		return "'"+input+"'";
	}
	
	/**
	 * To update data in databaseFacade.
	 * @param userName is user name.
	 * @param password is password.
	 * @return status of this update.
	 */
	public LoginResponseStatus update(String userName, String password){
		int o = databaseFacade.update(tableName, "password = "+surround(password), "userName ="+surround(userName));
		if(o == 1)
			return AUTH_OKAY;
		return AUTH_FAIL_USER;
	}
	
	/**
	 * To check data in databaseFacade.
	 * @param userName is user name.
	 * @param password is password.
	 * @return status of checkPass.
	 */
	public LoginResponseStatus checkPass(String userName, String password){
		List<Map> list = databaseFacade.select(tableName, "*", "WHERE userName = "+surround(userName));
		if(list.size()==0){
			return AUTH_FAIL_PASSWORD;
		}
		Map m = list.get(0);
			if(((String)m.get("password")).equals(password)){
				return AUTH_OKAY;
			}
		return AUTH_FAIL_PASSWORD;
	}
	
	/**
	 * To change the password.
	 * @param userName is user name.
	 * @param password is password.
	 * @return update of change password.
	 */
	public int changePass(String userName, String password){
		return databaseFacade.update(tableName, "password = '"+password+"'", "userName = '"+userName+"'");
	}
}
