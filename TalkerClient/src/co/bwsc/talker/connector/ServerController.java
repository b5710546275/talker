package co.bwsc.talker.connector;

import java.io.IOException;

import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.request.RequestManager;
import co.bwsc.talker.responseHandler.ResponseManager;

public class ServerController {
	private Server server;
	private RequestManager requestManager;
	public ServerController(String host,int port , ResponseManager responseManager) {
		System.out.println(host+" "+port);
		server = new Server(host, port,responseManager);
		requestManager = new RequestManager();
	}
	public int sentRequest(Request request){
		return requestManager.sendRequest(request, server);
	}
	public Server getServer() {
		return server;
	}
	public RequestManager getRequestManager() {
		return requestManager;
	}
	public void connect(){
		try {
			this.server.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
