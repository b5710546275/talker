package co.bwsc.talker.connector;

import java.io.IOException;

import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.responseHandler.ResponseManager;

import com.lloseng.ocsf.client.AbstractClient;

public class Server extends AbstractClient {
	ResponseManager responseManager;
	public Server(String host, int port, ResponseManager responseManager) {
		super(host, port);
		this.responseManager = responseManager;
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println(msg);
		responseManager.handleResponse(msg);
	}

	public int sentToServer(Request request){
		try {
			sendToServer(request);
		} catch (IOException e) {
			return -1;
		}
		return 0;
	}
}
