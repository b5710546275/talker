package co.bwsc.talker.request;

import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.connector.Server;

public class RequestManager {
	
	public int sendRequest(Request request, Server server) {
		return server.sentToServer(request);
	}

}
