package co.bwsc.talker.request;


import sun.security.util.Password;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.request.AddFriendRequest;
import co.bwsc.talker.base.request.InfoRequest;
import co.bwsc.talker.base.request.InfoRequestType;
import co.bwsc.talker.base.request.LoginRequest;
import co.bwsc.talker.base.request.PasswordChangeRequest;
import co.bwsc.talker.base.request.ProfileUpdateRequest;
import co.bwsc.talker.base.request.SendMessageRequest;

public class RequestFactory {
	public LoginRequest requestLogin(String username,String password){
		LoginRequest loginRequest = new LoginRequest(username, password);
		return loginRequest;
		
	}
	public AddFriendRequest requestAddFriend(int friendID){
		AddFriendRequest addFriendRequest = new AddFriendRequest(friendID);
		return addFriendRequest;
	}
	public InfoRequest requestInfo(InfoRequestType infoType){
		InfoRequest infoRequest = new InfoRequest(infoType);
		return infoRequest;
	}
	public InfoRequest requestInfo(InfoRequestType infoType, String message){
		InfoRequest infoRequest = new InfoRequest(infoType,message);
		return infoRequest;
	}
	public ProfileUpdateRequest requstUpdateProfile(Profile profile){
		ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest(profile);
		return profileUpdateRequest;
	}
	public PasswordChangeRequest requestPasswordChange(String newPassword){
		PasswordChangeRequest passwordChangeRequest = new PasswordChangeRequest(newPassword);
		return passwordChangeRequest;
	}
	public SendMessageRequest requestSendMessage(int targetChatId,Message message){
		SendMessageRequest sendMessageRequest = new SendMessageRequest(targetChatId, message);
		return sendMessageRequest;
	}
}
