package co.bwsc.talker.client;

import co.bwsc.talker.client.ui.GUIEngine;

public class Main {
	public static void main(String[] args) {
		System.out.println(Main.class.getName() + " | Firing " + GUIEngine.class.getName() + "...");
		GUIEngine.invokeGUI(args);
	}
}
