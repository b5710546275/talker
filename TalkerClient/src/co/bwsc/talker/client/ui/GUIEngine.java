package co.bwsc.talker.client.ui;

public class GUIEngine {
	public static void invokeGUI(String[] args) {
		System.out.println(GUIEngine.class.getName() + " | Launching " + LoginPage.class.getName() + "...");
		LoginPage.invoke(args);
	}
}
