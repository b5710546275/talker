package co.bwsc.talker.client.ui;

import java.util.ArrayList;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.User;
import co.bwsc.talker.base.UserState;
import co.bwsc.talker.base.UserStatus;

public class TestObjects {
	public static User user;
	public static Profile profile;
	public static ArrayList<User> users;
	public static ArrayList<Chat> chatList;
	public static ArrayList<Message> messageList;
	public static void main(String[] args) {
		chatList = new ArrayList<Chat>();
		user = new User(0, "User", UserState.NORMAL, UserStatus.AVAILABLE, 0);
		profile = new Profile(0, "USERDISP0", "FIRST0", "LAST0", "BIO0");
		chatList.add(new Chat(0));
		chatList.add(new Chat(1));
		chatList.add(new Chat(2));
		chatList.add(new Chat(3));
	}
}
