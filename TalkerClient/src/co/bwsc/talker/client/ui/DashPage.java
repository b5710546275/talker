package co.bwsc.talker.client.ui;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DashPage extends Application {

//	@FXML
//	private TextField usernameField, passwordField;
	Scene scene;
	Stage stage;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(
				"DashPage.fxml"));
		scene = new Scene(root, 800, 600);
		this.stage = stage;
		stage.setTitle("Talker");
		stage.setScene(scene);
		stage.show();
	}


//	@FXML
//	private void handleSignUp() {
//
//	}
//
//	@FXML
//	private void handleSettings() {
//
//	}

}
