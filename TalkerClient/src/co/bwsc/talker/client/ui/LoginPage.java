package co.bwsc.talker.client.ui;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginPage extends Application {

	public static void invoke(String[] args) {
		System.out.println(LoginPage.class.getName()
				+ " | JavaFX application launching...");
		launch(args);
	}

	@FXML
	private Parent root;
	@FXML
	private Button signInButton;
	@FXML
	private TextField usernameField, passwordField;

	@Override
	public void start(Stage stage) throws IOException {
		System.out.println(getClass().getName()
				+ " | FXMLLoader - Parsing FXML...");
		Parent root = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
		Scene scene = new Scene(root, 800, 600);

		stage.setTitle("Talker");
		stage.setScene(scene);
		stage.show();
		System.out.println(getClass().getName()
				+ " | Stage preparation finished.");
	}

	@FXML
	private void handleSignIn(ActionEvent event) throws IOException {
		boolean signInSuccessful = true;

		if (signInSuccessful) {
			System.out.println(getClass().getName() + " | Navigating to "
					+ DashPage.class.getName() + "...");
			new DashPage().start((Stage) root.getScene().getWindow());
		}
	}

	@FXML
	private void handleSignUp() {
		System.out.println(this.getClass().getName() + " | handleSignUp()");
	}

	@FXML
	private void handleSettings() {
		System.out.println(this.getClass().getName() + " | handleSettings()");
	}
}
