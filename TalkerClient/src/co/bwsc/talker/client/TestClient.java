package co.bwsc.talker.client;

import java.io.IOException;

import co.bwsc.talker.base.request.LoginRequest;

import com.lloseng.ocsf.client.AbstractClient;

public class TestClient extends AbstractClient{

	public TestClient(String host, int port) {
		super(host, port);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		TestClient clientTest = new TestClient( "127.0.0.1", 5000);
		try {
			clientTest.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			clientTest.sendToServer("hello");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			clientTest.sendToServer(new LoginRequest("tor", "noop"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
