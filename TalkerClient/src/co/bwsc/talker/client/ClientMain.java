package co.bwsc.talker.client;

import java.util.Date;

import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.MessageType;

public class ClientMain {
	public static void main(String[] args) {
		Client client = new Client();
		System.out.println("client 1");
		client.connectToHost();

		client.login("apple", "apple");
		client.login("apple", "1234");

		client.getUserFromServer();

		client.getChat();
		
		client.getProfile();

		client.getFriends();
		
		client.getMessage(110000);
		
		Message message = new Message(new Date(System.currentTimeMillis()), 100000002, "hello", MessageType.PLAIN, "");
		client.sendMessage(110000, message);
	}
}
