package co.bwsc.talker.client;

import java.util.List;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.User;
import co.bwsc.talker.base.request.InfoRequestType;
import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.base.response.LoginResponseStatus;
import co.bwsc.talker.connector.ServerController;
import co.bwsc.talker.request.RequestFactory;
import co.bwsc.talker.request.RequestManager;
import co.bwsc.talker.responseHandler.ResponseManager;


public class Client {
	private ServerController serverController;
	private RequestManager requestManager;
	private RequestFactory requestFactory;
	private ResponseManager responseManager = new ResponseManager(this);
	private User user;
	private List<Chat> chats;
	private List<Profile> friends;
	private boolean loggedIn = false;

	Client() {
		serverController = new ServerController("localhost", 5555,
				responseManager);
		requestFactory = new RequestFactory();
	}

	public void connectToHost() {
		serverController.connect();
	}

	public void login(String username, String password) {
		Request request = requestFactory.requestLogin(username, password);
		sentRequest(request);
	}

	public void getUserFromServer() {
		sentRequest(InfoRequestType.USER);
	}

	public void getChat() {
		sentRequest(InfoRequestType.CHATS);
	}

	public void getMessage(int chatID) {
		sentInfoRequest(InfoRequestType.MESSAGES_IN_CHAT,
				String.valueOf(chatID));
	}

	public void sentRequest(Request request) {
		serverController.sentRequest(request);
	}

	public void sentInfoRequest(InfoRequestType inforequestType, String msg) {
		Request request = requestFactory.requestInfo(inforequestType, msg);
		serverController.sentRequest(request);
	}

	public void sentRequest(InfoRequestType inforequestType) {
		Request request = requestFactory.requestInfo(inforequestType);
		serverController.sentRequest(request);
	}

	public void getFriends() {
		sentRequest(InfoRequestType.FRIENDS);
	}

	public void getUserInChat(int chatID) {
		sentInfoRequest(InfoRequestType.USERS_IN_CHAT, String.valueOf(chatID));
	}

	public void getMedia(int messageID) {
		sentInfoRequest(InfoRequestType.MEDIA_IN_MESSAGE,
				String.valueOf(messageID));
	}

	public void getProfile() {
		sentRequest(InfoRequestType.PROFILE);
	}

	public void getProfile(int userID) {
		sentInfoRequest(InfoRequestType.PROFILE, String.valueOf(userID));

	}

	public boolean isLogined() {
		return loggedIn;
	}

	public void setLogined(boolean logined) {
		this.loggedIn = logined;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void canNotLogin(LoginResponseStatus loginResponseStatus) {
		System.out.println("can not login :" + loginResponseStatus);
	}

	public void passwordChanged() {
		System.out.println("password change");
	}

	public User getUser() {
		return user;
	}

	public List<Chat> getChats() {
		return chats;
	}

	public void setChats(List<Chat> chats) {
		this.chats = chats;
	}

	public void setFriends(List<Profile> friends) {
		this.friends = friends;
	}

	public void sendMessage(int targetChatId, Message message) {
		Request request = requestFactory.requestSendMessage(targetChatId,
				message);
		serverController.sentRequest(request);
	}
	
}
