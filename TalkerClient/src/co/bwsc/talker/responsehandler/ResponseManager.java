package co.bwsc.talker.responseHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.response.AddFriendResponseStatus;
import co.bwsc.talker.base.response.ChatResponseStatus;
import co.bwsc.talker.base.response.FriendResponseStatus;
import co.bwsc.talker.base.response.LoginResponseStatus;
import co.bwsc.talker.base.response.MediaResponseStatus;
import co.bwsc.talker.base.response.MessageOneResponseStatus;
import co.bwsc.talker.base.response.MessageResponseStatus;
import co.bwsc.talker.base.response.PasswordChangeResponseStatus;
import co.bwsc.talker.base.response.ProfileResponseStatus;
import co.bwsc.talker.base.response.UserInChatResponseStatus;
import co.bwsc.talker.base.response.UserResponseStatus;
import co.bwsc.talker.client.Client;

public class ResponseManager  {
	private static Client client;
	public ResponseManager(Client client) {
		this.client = client;
	}
	enum type {
		LoginResponse {
			@Override
			void handle(Object msg) {
				System.out.println("login");
				System.out.println("================================================");
				
				co.bwsc.talker.base.response.LoginResponse loginResponse = (co.bwsc.talker.base.response.LoginResponse) msg;
				LoginResponseStatus loginResponseStatus =loginResponse.getResponseStatus();
				if(loginResponseStatus == LoginResponseStatus.AUTH_OKAY){
					client.setLogined(true);
				}
				else{
					client.canNotLogin(loginResponseStatus);
				}
				System.out.println(loginResponseStatus);
			}
		},
		PasswordChangeResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.PasswordChangeResponse passwordChangeResponse = (co.bwsc.talker.base.response.PasswordChangeResponse)msg;
				PasswordChangeResponseStatus passwordChangeResponseStatus = passwordChangeResponse.getResponseStatus();
				if(passwordChangeResponseStatus == PasswordChangeResponseStatus.CHANGE_SUCCESSFUL){
					
				}
				else{
					
				}
			}
		},
		UserResponse {
			@Override
			void handle(Object msg) {
				System.out.println("Get User");
				System.out.println("================================================");
				
				co.bwsc.talker.base.response.UserResponse userResponse = (co.bwsc.talker.base.response.UserResponse)msg;
				UserResponseStatus userResponseStatus = userResponse.getResponseStatus();
				if(userResponseStatus == UserResponseStatus.NORMAL){
					client.setUser(userResponse.getResponseObject());
					System.out.println(client.getUser());
				}
				else{
					
				}
			}
		},
		ProfileResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.ProfileResponse profileResponse = (co.bwsc.talker.base.response.ProfileResponse)msg;
				ProfileResponseStatus profileResponseStatus = profileResponse.getResponseStatus();
				if(profileResponseStatus == ProfileResponseStatus.NORMAL){
					System.out.println(profileResponse.getResponseObject());
				}
				else{
					
				}
			}
		},
		ChatResponse {
			@Override
			void handle(Object msg) {
				System.out.println("Get Chats");
				System.out.println("================================================");
				
				co.bwsc.talker.base.response.ChatResponse chatResponse = (co.bwsc.talker.base.response.ChatResponse)msg;
				ChatResponseStatus chatResponseStatus = chatResponse.getResponseStatus();
				if(chatResponseStatus == ChatResponseStatus.NORMAL){
					List<Chat> newChatList = new ArrayList<Chat>();
					List chatAsList = Arrays.asList(chatResponse.getResponseObject());
					newChatList.addAll(chatAsList);
					client.setChats(newChatList);
					System.out.println(client.getChats());
				}
				else{
					
				}
			}
		},
		MediaResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.MediaResponse mediaResponse = (co.bwsc.talker.base.response.MediaResponse)msg;
				MediaResponseStatus mediaResponseStatus = mediaResponse.getResponseStatus();
				if(mediaResponseStatus == MediaResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
		},
		MessageResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.MessageResponse messageResponse = (co.bwsc.talker.base.response.MessageResponse)msg;
				MessageResponseStatus messageResponseStatus = messageResponse.getResponseStatus();
				if(messageResponseStatus == MessageResponseStatus.NORMAL){
					
				}
				else{
					
				}
				System.out.println(Arrays.toString(messageResponse.getResponseObject()));
			}
		},
		FriendResponse {
			
			@Override
			void handle(Object msg){
				co.bwsc.talker.base.response.FriendResponse friendResponse = (co.bwsc.talker.base.response.FriendResponse)msg;
				FriendResponseStatus friendResponseStatus = friendResponse.getResponseStatus();	
				if(friendResponseStatus == FriendResponseStatus.NORMAL){
					
				}
				else{
					
				}
				System.out.println(Arrays.toString(friendResponse.getResponseObject()));
			}
		},
		AddFriendResponse{

			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.AddFriendResponse addFriendResponse = (co.bwsc.talker.base.response.AddFriendResponse)msg;
				AddFriendResponseStatus addFriendResponseStatus = addFriendResponse.getResponseStatus();
				if(addFriendResponseStatus == AddFriendResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
			
		},
		MessageOneResponse{

			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.MessageOneResponse messageOneResponse = (co.bwsc.talker.base.response.MessageOneResponse)msg;
				MessageOneResponseStatus messageOneResponseStatus = messageOneResponse.getResponseStatus();
				if(messageOneResponseStatus == MessageOneResponseStatus.NORMAL){
					
				}
				else{
					
				}
				System.out.println(messageOneResponse.getResponseObject());
			}
			
		},
		UserInChatResponse{

			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.UserInChatResponse userInChatResponse =(co.bwsc.talker.base.response.UserInChatResponse)msg;
				UserInChatResponseStatus userInChatResponseStatus = userInChatResponse.getResponseStatus();
				if(userInChatResponseStatus == UserInChatResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
			
		}
		;
		abstract void handle(Object msg);
	}
	public void handleResponse(Object msg){
		System.out.println("here : "+msg.getClass().getSimpleName().toString());
		type.valueOf(msg.getClass().getSimpleName().toString()).handle(msg);
	}
}
